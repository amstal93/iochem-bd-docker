##
## Mail setup configuration parameters
##

# Set the address the emails will be send from (required)
# Must be set even if the email service isn't configured
mailFromAddress = whatever@example.com

#Mail servername or IP (optional)
mailServer = smtp.foobar.com

#Mail account username and password (optional)
mailServerUsername = username
mailServerPassword = password

#Port used by mail server (optional)
mailServerPort = 25

#Mail server uses SSL or STARTTLS 
mailIsSSL = false
mailIsSTARTTLS = false


##
## Web service configuration
##

# The hostname parameter sets the servername of the ioChem-BD service (required)
# This value must be set also on /etc/hosts with the localhost address 
# to avoid internal ioChem-BD services to search out of the machine, ex:
#
# [/etc/hosts]
# 127.0.0.1 test.iochem-bd.org
hostName = test.iochem-bd.org


# Public IP of the machine, only in case it's open to the world, (required)
# In case it is used on a private network set the internal IP and
# when used locally use the 127.0.0.1 value
hostIp	 = 127.0.0.1

# Port number where the ioChem-BD service will run, if set on privileged ports (under 1024),
# a script must be run after installation. It's located on base installation folder and called postinstall.sh 
hostPort = 8443

##
## HTTPS certificate fields
## 

# Following fields will be used to generate a self-signed certificate that is needed for Tomcat to deliver the HTTPS service
# organizationName is the only (required)
organizationName  = Foo Bar organization
organizationUnit = Foo Bar deparment
city = Springfield
state = Springfield's State
countryCode = US


##
## Database connection fields
## 

# In order to connect the database all the following fields are (required)
# Two databases must be created "iochemCreate" and "iochemBrowse", read the documentation for it.
# It is advised to check connection trying to connect to the databases prior running the installation process.
# Example:  
#        $ psql -U iochembd -W -h localhost "iochemCreate"
#        $ psql -U iochembd -W -h localhost "iochemBrowse"
#
# databaseHost parameter can be a hostname or an IP

databasePort = 5432
databaseHost = localhost
databaseUser = iochembd
databasePwd  = iochembd
databaseIsSSL = false

##
## Administrator account setup
##

# The initial user is also the administrator of the ioChem-BD platform, its credentials will appear here.
# Both fields are (required), once installed adminpassword parameter should be removed.
adminEmail = whatever@example.com
adminPassword = mypassword
